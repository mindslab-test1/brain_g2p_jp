var fs = require('fs');
var input_list = fs.readFileSync('/dev/stdin').toString().split('\n')
//input_list = [ input_list[0] ];

var Kuroshiro = require("kuroshiro");
const kuroshiro = new Kuroshiro();
var KuromojiAnalyzer = require("kuroshiro-analyzer-kuromoji");

kuroshiro.init(new KuromojiAnalyzer())
    .then(async function () {
                s = [];
                for(const input of input_list) {
                    x = await kuroshiro.convert(input, { to: "romaji", romajiSystem: "hepburn", mode: "spaced" });
                    s.push(x);
                }
                return s;
    })
    .then(function(s){
        for(const result of s) {
                console.log(result);
        }
    })
