cat $1
echo "mecab:  "
python mecab.py < $1
echo ""

echo "jphones:  "
python jphones-run.py < $1
echo ""

echo "kakasi:  "
python kakasi.py < $1
echo ""

echo "romanizer:  "
node romanizer.js < $1
echo ""

echo "kuroshiro:  "
node kuroshiro.js < $1
echo ""

echo "kanji to romaji:  "
python kanji2romaji.py < $1
echo ""
