import pykakasi
from sys import stdin

s = ''
for l in stdin:
    s += l 

kakasi = pykakasi.kakasi()
kakasi.setMode('H','a')
kakasi.setMode('K','a')
kakasi.setMode('J','a')
kakasi.setMode('r','Hepburn')
kakasi.setMode("s", True)
kakasi.setMode("C", False) #True)
conv = kakasi.getConverter()
print(conv.do(s))
