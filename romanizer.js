var fs = require('fs');
var input_list = fs.readFileSync('/dev/stdin').toString().split('\n')
//input_list = [ input_list[0] ];

var romanizer= require('romanizer');

var rep_dict = { 'ā':'aa', 'ī':'ii', 'ū':'uu', 'ē':'ee', 'ō':'ou', 'â':'aa', 'î':'ii', 'û':'uu', 'ê':'ee', 'ô':'oo' }

for(const input of input_list) {
    romanizer.romanize(input).then(function(romaji) {
        for(let rep in rep_dict) {
            romaji = romaji.split(rep).join(rep_dict[rep]);
        }
        console.log(romaji);// nihongo de ok
    });
}
