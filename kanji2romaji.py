from kanji_to_romaji import kanji_to_romaji
from sys import stdin

s = ''
for l in stdin:
    s += l

print(kanji_to_romaji(s))
