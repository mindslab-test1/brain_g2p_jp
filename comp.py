with open('basic5000_ans','r') as f:
    ans = f.readlines()

with open('basic5000_mecab_output','r') as f:
    mecab = f.readlines()

with open('basic5000_kuroshiro_output','r') as f:
    krsr = f.readlines()

with open('basic5000_kakasi_output','r') as f:
    kks = f.readlines()

with open('basic5000_romanizer_output','r') as f:
    rmnz = f.readlines()

with open('basic5000_jphones_output','r') as f:
    jph = f.readlines()

#"""
def calc_cache_pos(strings, indexes):
    factor = 1
    pos = 0
    for s, i in zip(strings, indexes):
        pos += i * factor
        factor *= len(s)
    return pos

def lcs_back(strings, indexes, cache):
    if -1 in indexes:
        return ""
    match = all(strings[0][indexes[0]] == s[i]
                for s, i in zip(strings, indexes))
    if match:
        new_indexes = [i - 1 for i in indexes]
        result = lcs_back(strings, new_indexes, cache) + strings[0][indexes[0]]
    else:
        substrings = [""] * len(strings)
        for n in range(len(strings)):
            if indexes[n] > 0:
                new_indexes = indexes[:]
                new_indexes[n] -= 1
                cache_pos = calc_cache_pos(strings, new_indexes)
                if cache[cache_pos] is None:
                    substrings[n] = lcs_back(strings, new_indexes, cache)
                else:
                    substrings[n] = cache[cache_pos]
        result = max(substrings, key=len)
    cache[calc_cache_pos(strings, indexes)] = result
    return result

def lcs(a, b):
    strings = [a,b]
    # >>> lcs(['666222054263314443712', '5432127413542377777', '6664664565464057425'])
    # '54442'
    # >>> lcs(['abacbdab', 'bdcaba', 'cbacaa'])
    # 'baa'
    if len(strings) == 0:
        return ""
    elif len(strings) == 1:
        return strings[0]
    else:
        cache_size = 1
        for s in strings:
            cache_size *= len(s)
        cache = [None] * cache_size
        indexes = [len(s) - 1 for s in strings]
        return lcs_back(strings, indexes, cache)
#"""
"""
def lcs(a, b):
    # generate matrix of length of longest common subsequence for substrings of both words
    lengths = [[0] * (len(b)+1) for _ in range(len(a)+1)]
    for i, x in enumerate(a):
        for j, y in enumerate(b):
            if x == y:
                lengths[i+1][j+1] = lengths[i][j] + 1
            else:
                lengths[i+1][j+1] = max(lengths[i+1][j], lengths[i][j+1])
 
    # read a substring from the matrix
    result = ''
    j = len(b)
    for i in range(1, len(a)+1):
        if lengths[i][j] != lengths[i-1][j]:
            result += a[i-1]
 
    return result
"""

for i in range(len(ans)):
    ans[i] = ans[i].replace('-','').replace('pau','').replace('N','n').replace('cl','t')
    mecab[i] = mecab[i].replace('\'','').replace('、','')
    krsr[i] = krsr[i].replace(',','')
    kks[i] = kks[i].replace('、','')
    rmnz[i] = rmnz[i].replace('\'','').replace(',','')
    jph[i] = jph[i].replace('、','')

    
    print('ans:\t\t',ans[i],end='')
    print('mecab:\t\t',mecab[i],end='')
    print('kuroshiro:\t',krsr[i],end='')
    print('kakasi:\t\t',kks[i],end='')
    print('romanizer:\t',rmnz[i],end='')
    print('jphones:\t',jph[i],end='')
    
    ans[i] = " ".join(ans[i])
    jph[i] = " ".join(jph[i])

        
    """
    l = lcs(ans[i], mecab[i])
    print(l)
    l = lcs(l, krsr[i])
    print(l)
    l = lcs(l, kks[i])
    print(l)
    l = lcs(l, rmnz[i])
    print(l)
    l = lcs(l, jph[i])
    print(l)
    print('=========')
    """
    #l = lcs(rmnz[i], jph[i])
    #l = lcs(l, kks[i])
    #l = lcs(l, krsr[i])
    #l = lcs(l, mecab[i])
    #l = lcs(l, ans[i])
    #print('lcs:\t\t',l)
    #print('lcs:\t\t',l.replace(' ',''),end='')
    #print('ans:\t\t',ans[i].replace(' ',''))

    rep_dict = {' ':'', 'ā':'aa','ē':'ee','ī':'ii','ō':'oo','ū':'uu', 'nanino':'nanno', '.':'' }

    for key in rep_dict:
        krsr[i] = krsr[i].replace(key,rep_dict[key])
    
    print()
    print('kuroshiro:\t',krsr[i],end='')
    print('ans:\t\t',ans[i].replace(' ',''))
    print(krsr[i] == ans[i].replace(' ',''))

    print()
